import React, { useState } from 'react'
import { Card, Input, Space, Button, Col, Row } from 'antd';
import axios from "axios";
import StudentsHome from '../student/Home';
import FacultyHome from "../faculty/Home";
import AdminHome from "../admin/Home";




const Login = () => {
    const [TEXT1, SETTEXT1] = useState("stud_2");
    const [TEXT2, SETTEXT2] = useState("1");
    const [key, KEY] = useState(0);
    const [loginDetails, loginDetailsSet] = useState(0);



    const registerno = (e) => {
        SETTEXT1(e.target.value);
    }
    const password = (e) => {
        SETTEXT2(e.target.value);
    }
    const ButtonClick = () => {
        let data = { _id : TEXT1, PASSWORD: TEXT2 }
        if (!TEXT1) {
            alert("ENTER YOUR CORRECT REGISTER NO");
        }
        else if (!TEXT2) {
            alert('EENTER YOUR PASSWORD ');
        }
        else {
            axios.post("http://127.0.0.1:7000/login/check", data).then((res) => {
                console.log(res.data.result);
                if (res.data.Status === 1) {
                    loginDetailsSet(res.data.result);
                    alert("successful");
                    if (res.data.result[0].ROLE === 'A') {
                        KEY(1)
                    } else if (res.data.result[0].ROLE === 'F') {
                        KEY(2);
                    } else if (res.data.result[0].ROLE === 'S') {
                        KEY(3);
                    }

                } else {
                    alert("invalid");
                }
            });
        }
    }
    return (
        <div className='card-designss'>
            {
                key === 0 ? (
                    <Row gutter={[12, 12]} justify="center" style={{ marginTop: "0%" }}>

                        <div>
                            <Card
                                className='card-design'
                                title="LOGIN"
                                extra={<a href='#' style={{ color: "red",fontWeight:"bold"}} >Forgot password?</a>}
                                bordered={true}
                                style={{
                                    width: 300, marginTop: "20%"
                                }}
                            >  
                                <Row gutter={[12, 12]} justify="start">

                                 REGISTER ID :   <Input placeholder="REGISTER ID" onChange={registerno} />
                                   PASSWORD : <Input placeholder="PASSWORD" onChange={password} />
                                    <Button block type="primary" onClick={ButtonClick}>Sign In</Button>
                                </Row>


                            </Card>
                            
                        </div>
                    </Row>) : key === 3 ? (<StudentsHome details={loginDetails}></StudentsHome>) :
                    key === 2 ? (<FacultyHome details={loginDetails}></FacultyHome>) :
                        key === 1 ? (<AdminHome details={loginDetails}></AdminHome>) : ""
            }
        </div>
    );
}

export default Login;
