import React, { useState, useEffect} from 'react';
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    DeleteOutlined,
    EditOutlined,
    UserAddOutlined,
    LogoutOutlined 

} from '@ant-design/icons';
import { Layout, Menu, Descriptions } from 'antd';
import VIEW from './View';
const { Header, Sider, Content } = Layout;

const Home = (props) => {
    const [collapsed, setCollapsed] = useState(false);
    const [key, setCurrent] = useState();

    useEffect(() => {
        let data = { details: props.details }
        console.log(data);
    });
    const menuClick = (e) => {
        console.log(e.key)
        setCurrent(parseInt(e.key));


    };

    return (
        <div> <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo" />
                <Menu
                    theme="light"
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    onClick={menuClick}
                    items={[
                        {
                            key: '1',
                            icon: <UserOutlined />,
                            label: 'Home'
                        },
                        {
                            key: '2',
                            icon: <VideoCameraOutlined />,
                            label: 'View entry',
                        },
                        {
                            key: '3',
                            icon: <UserAddOutlined />,
                            label: 'Add entry',
                        },
                        {
                            key: '4',
                            icon: <EditOutlined />,
                            label: 'update entry',
                        },
                        {
                            key: '5',
                            icon: <DeleteOutlined />,
                            label: 'Delete entry',
                        },
                        {
                            key: '6',
                            icon: <LogoutOutlined />,
                            label: 'Sign out',
                        },
                    ]}
                />
            </Sider>
            <Layout className="site-layout">
                <Header
                    className="site-layout-background"
                    style={{
                        padding: 0,
                    }}
                >
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: () => setCollapsed(!collapsed),
                    })}
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    {
                        key === 1 ? (<Descriptions title="User Info" bordered>
                            <Descriptions.Item label="_id" span={4}>{props.details[0]._id}</Descriptions.Item>
                            <Descriptions.Item label="FIRST_NAME" span={2}>{props.details[0].FIRST_NAME}</Descriptions.Item>
                            <Descriptions.Item label="SECOND_NAME" span={2}>{props.details[0].SECOND_NAME}</Descriptions.Item>
                            <Descriptions.Item label="MAIL_ID" span={4}>{props.details[0].MAIL_ID}</Descriptions.Item>
                            <Descriptions.Item label="MOBILE" span={4}>
                                {props.details[0].MOBILE}
                            </Descriptions.Item>
                            <Descriptions.Item label="ROLE" >{props.details[0].ROLE}</Descriptions.Item>
                            <Descriptions.Item label="DEPARTMENT ">{props.details[0].DEPARTMENT}</Descriptions.Item>
                            <Descriptions.Item label="YEAR">{props.details[0].YEAR}</Descriptions.Item>
                            <Descriptions.Item label="SECTION">{props.details[0].SECTION}</Descriptions.Item>
                            <Descriptions.Item label="PASSWORD">{props.details[0].PASSWORD}</Descriptions.Item>
                        </Descriptions>) : key === 2 ? (<VIEW></VIEW>
                        ) : ""
                    }                </Content>
            </Layout>
        </Layout>

        </div>
    )
}

export default Home

