import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Input, Button, DatePicker, TimePicker } from 'antd';
import axios from "axios";
import moment from 'moment';

const View = (props) => {

    const [TEXT1, SETTEXT1] = useState("");
    const [TEXT2, SETTEXT2] = useState("");
    const [TEXT3, SETTEXT3] = useState("");
    const [TEXT4, SETTEXT4] = useState("");
    const [TEXT5, SETTEXT5] = useState("");
    const [TEXT6, setValue] = useState(null);
    const [TEXT7, SETTEXT7] = useState("2022/01/01");
    const dateFormat = 'YYYY/MM/DD';

    const text1BoxonChange = (e) => {
        SETTEXT1(e.target.value);
    }

    const text2BoxonChange = (e) => {
        SETTEXT2(e.target.value);
    }
    const text3BoxonChange = (e) => {
        SETTEXT3(e.target.value);
    }
    const text4BoxonChange = (e) => {
        SETTEXT4(e.target.value);
    }
    const text5BoxonChange = (e) => {
        SETTEXT5(e.target.value);
    }
    const text6BoxonChange = (time) => {
        let t = moment(time).format('h:mm:ss a');
        setValue(t);
    };
    const text7BoxonChange = (date, dateString) => {
        console.log(date, dateString);
        SETTEXT7(dateString);
    };

    const ButtonClick = () => {
        let _id = TEXT1;
        let DOSE_NUMBER = TEXT2;
        let HOSPITAL = TEXT3;
        let HOSPITAL_id = TEXT4;
        let PLACE = TEXT5;
        let TIME = TEXT6;
        let DATE = TEXT7;





        let data = {
            _id: props.details._id,
           "dose_details": { _id: TEXT1, DOSE_NUMBER: TEXT2, HOSPITAL: TEXT3, HOSPITAL_id: TEXT4, PLACE: TEXT5, TIME: TEXT6, DATE: TEXT7 }}
        console.log(data);

        axios.post("http://127.0.0.1:7000/student/insertnew", data).then((res) => {
            console.log(res.data);
            if (res.data.Status === 1) {
                alert("successful");

            } else {
                alert("invalid");
            }
        });

    }


    return (
        <div>
            <Card
                title="ADD NEW"
                style={{
                    width: "100%",
                }}
            >

                <Row gutter={[12, 12]} justify="start">
                    <Col span={24}>
                        <Input placeholder="_id" onChange={text1BoxonChange} value={TEXT1} />
                    </Col>
                    <Col span={24}>
                        <Input placeholder="DOSE NUMBER" onChange={text2BoxonChange} value={TEXT2} />
                    </Col>
                    <Col span={24}>
                        <Input placeholder="HOSPITAL NAME" onChange={text3BoxonChange} value={TEXT3} />


                    </Col>
                    <Col span={24}>
                        <Input placeholder="HOSPITAL ID" onChange={text4BoxonChange} value={TEXT4} />
                    </Col>

                    <Col span={24}>
                        <Input placeholder="PLACE" onChange={text5BoxonChange} value={TEXT5} />
                    </Col>
                    <Col span={24}>
                        <TimePicker onChange={text6BoxonChange} />
                    </Col>
                    <Col span={24}>

                        <DatePicker style={{ width: '100%' }} placeholder="D.O.B" onChange={text7BoxonChange}

                            defaultValue={moment(TEXT7, dateFormat)} format={dateFormat} />   </Col>
                    <Col span={24}>
                        <Button block type="primary" onClick={ButtonClick}>ADD INFO</Button>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default View
