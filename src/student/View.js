import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import axios from "axios";


const columns = [
  {
    title: 'ID',
    dataIndex: '_id',
    key: '_id',
  },
  {
    title: 'Dose Number',
    dataIndex: 'DOSE_NUMBER',
    key: 'DOSE_NUMBER',
  },
  {
    title: 'Hospital',
    dataIndex: 'HOSPITAL',
    key: 'HOSPITAL',
  },

  {
    title: 'Hospital ID',
    dataIndex: 'HOSPITAL_id',
    key: 'HOSPITAL_id',
  },
  {
    title: 'Place',
    dataIndex: 'PLACE',
    key: 'PLACE',
  },
  {
    title: 'Time',
    dataIndex: 'TIME',
    key: 'TIME',
  }

];



const View = (props) => {
  const [output, result] = useState();
  useEffect(() => {
    console.log(props)
    let data =
    {
      _id: props.details._id
    }

    console.log(data)
    axios.post("http://127.0.0.1:7000/common/view", data).then((res) => {
      console.log(res.data.result);
      result(res.data.result);
    });
  }, []);

  return (
    <div>
      <Table columns={columns} dataSource={output} />
    </div>
  )
}

export default View;