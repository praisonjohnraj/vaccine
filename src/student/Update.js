import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Input, Button } from 'antd';
import axios from "axios";


const View = (props) => {
    const [TEXT1, SETTEXT1] = useState("");
    const [TEXT2, SETTEXT2] = useState("");
    const [TEXT3, SETTEXT3] = useState("");
    const [TEXT4, SETTEXT4] = useState("");
    const [TEXT5, SETTEXT5] = useState("");
    const [TEXT6, SETTEXT6] = useState("");
    const [TEXT7, SETTEXT7] = useState("");
    const [TEXT8, SETTEXT8] = useState("");
    const [TEXT9, SETTEXT9] = useState("");
    const [TEXT10, SETTEXT10] = useState("");

    useEffect(() => {
        console.log(props)

    }, []);


    const text1BoxonChange = (e) => {
        SETTEXT1(e.target.value);
    }

    const text2BoxonChange = (e) => {
        SETTEXT2(e.target.value);
    }
    const text3BoxonChange = (e) => {
        SETTEXT3(e.target.value);
    }
    const text4BoxonChange = (e) => {
        SETTEXT4(e.target.value);
    }
    const text5BoxonChange = (e) => {
        SETTEXT5(e.target.value);
    }

    const text6BoxonChange = (e) => {
        SETTEXT6(e.target.value);
    }

    const text7BoxonChange = (e) => {
        SETTEXT7(e.target.value);
    }
    const text8BoxonChange = (e) => {
        SETTEXT8(e.target.value);
    }
    const text9BoxonChange = (e) => {
        SETTEXT9(e.target.value);
    }
    const text10BoxonChange = (e) => {
        SETTEXT10(e.target.value);
    }

      
    const ButtonClick = () => {
        let _id = TEXT1;
        let FIRST_NAME = TEXT2;
        let SECOND_NAME = TEXT3;
        let MAIL_ID = TEXT4;
        let MOBILE = TEXT5;
        let ROLE = TEXT6;
        let DEPARTMENT = TEXT7;
        let YEAR = TEXT8;
        let SECTION = TEXT10;
        let PASSWORD = TEXT7;






        let data = 
        {
            _id: props.details._id, FIRST_NAME: TEXT2, SECOND_NAME: TEXT3, MAIL_ID: TEXT4, MOBILE: TEXT5, 
            ROLE: TEXT6, DEPARTMENT: TEXT7, YEAR: TEXT8, SECTION: TEXT9, PASSWORD:TEXT10
         }
        
        console.log(data);

        axios.post("http://127.0.0.1:7000/common/updateuser", data).then((res) => {
            console.log(res.data);
            if (res.data.Status === 1) {
                alert("successful");

            } else {
                alert("invalid");
            }
        });

    }


            return (
            <div>
                <Card
                    title="PROFILE UPDATE"
                    style={{
                        width: "100%",
                    }}
                >

                    <Row gutter={[12, 12]} justify="start">
                            <Col span={24}>
                            <Input placeholder="_id" onChange={text1BoxonChange} value={TEXT1} />
                        </Col>
                        <Col span={24}>
                                <Input placeholder="FIRST NAME" onChange={text2BoxonChange} value={TEXT2} />
                        </Col>
                        <Col span={24}>
                            <Input placeholder=" SECOND NAME" onChange={text3BoxonChange} value={TEXT3} />

                        </Col>
                        <Col span={24}>
                                <Input placeholder="MAIL ID" onChange={text4BoxonChange} value={TEXT4} />
                        </Col>
                            <Col span={24}>
                                <Input placeholder="MOBILE no" onChange={text5BoxonChange} value={TEXT5} />
                            </Col>
                            <Col span={24}>
                                <Input placeholder="ROLE" onChange={text6BoxonChange} value={TEXT6} />
                            </Col>
                            <Col span={24}>
                                <Input placeholder="DEPARTMENT" onChange={text7BoxonChange} value={TEXT7} />
                            </Col>

                            <Col span={24}>
                                <Input placeholder="YEAR" onChange={text8BoxonChange} value={TEXT8} />
                            </Col>

                            <Col span={24}>
                                <Input placeholder="SECTION" onChange={text9BoxonChange} value={TEXT9} />
                            </Col>

                            <Col span={24}>
                                <Input placeholder="PASSWORD" onChange={text10BoxonChange} value={TEXT10} />
                            </Col>


                        <Col span={24}>
                            <Button block type="primary" onClick={ButtonClick}>UPDATE USER</Button>
                        </Col>
                    </Row>
                </Card>
            </div>
            )
}

            export default View;
  