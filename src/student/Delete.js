import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Input, Button } from 'antd';
import axios from "axios";


const View = (props) => {
    const [TEXT1, SETTEXT1] = useState("");

    const text1BoxonChange = (e) => {
        SETTEXT1(e.target.value);
    }

    useEffect(() => {
        console.log(props)
 
    }, []);

    const ButtonClick = () => {
        let _id = TEXT1;

        let data =
        {
            "_id": props.details._id,
            "dose_details": {
                           "_id": _id
            }
        }
        console.log(data)
        axios.post("http://127.0.0.1:7000/common/deletedose", data).then((res) => {
            console.log(res.data);
            if (res.data.Status === 1) {
                alert("successful");

            } else {
                alert("invalid");
            }
        });

    }

    return (
        <div>
            <Card
                title="ADD NEW"
                style={{
                    width: "100%",
                }}
            >

                <Row gutter={[12, 12]} justify="start">
                    <Col span={24}>
                        <Input placeholder="DOSE NO" onChange={text1BoxonChange} value={TEXT1} />
                    </Col>
               
                    <Col span={24}>
                        <Button block type="primary" onClick={ButtonClick}>DELETE DOSE DETAILS</Button>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default View;